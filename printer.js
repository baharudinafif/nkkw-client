const _ = require('lodash');
const path = require('path');
const async = require('async');
const moment = require('moment-timezone');
const escpos = require('escpos');

moment.locale('id');

const { BARCODE_TYPE, TIME_ZONE, BARCODE_MAX_LENGTH } = process.env;

let device;

const setPrinter = () => {
	try {
		if (_.isNil(device)) {
			device = new escpos.USB();
			if (!_.isNil(device)) { console.log('Printer is connected!!!'); }
		}		
	} catch (error) {
		console.log(new Date(), { error: error.message }, !_.isEmpty(device));
	}
}
setInterval(setPrinter, 2000);

const options = {
	encoding: "GB18030" /* default */
}

const logoBig = path.join(__dirname, 'logo-big.jpg');
const logoSmall = path.join(__dirname, 'logo-small.png');


const MAX_BIG_CHAR = 44;
const MAX_SMALL_CHAR = 32;

buildTextTiket = ({ key = '', value = '' } = {}, ukuran = "besar") => {
	const MAX_CHAR 	= _.isEqual(ukuran, "besar") ? MAX_BIG_CHAR : MAX_SMALL_CHAR;
	const paddingLR = _.isEqual(ukuran, "besar") ? '  ' : '';
	const tab 			= '  ';
	const spacing 	= '     ';
	let kt = key.toString().trim();
	let kl = kt.length;
	const val = value.toString();

	const list = [];

	let remaining = MAX_CHAR;
	remaining -= (spacing.length + val.length);
	
	// console.log(remaining, kt, kl, key);
	while(kl > 0) {
		let max = remaining;
		if (kl === key.length) {
			let text = kt.padEnd(max, ' ');
			if (kt.length > max) { text.substring(0, max); }
			list.push([paddingLR, text, spacing, val, paddingLR].join(''));
		} else {
			list.push([paddingLR, tab, kt.substring(0, max).trim()].join(''));
		}
		if (kl > max) { kt = kt.substring(max); kl = kt.length; } else { kt = ""; kl = 0; }
	}
	// console.log(list);
	return list;
}

const buildTextPenjualan = ({ qty = '', name = '', nominal = '' }, ukuran = 'kecil') => {
	const MAX_CHAR = MAX_SMALL_CHAR;
	const MAX_VALUE_L = 9;
	const spacing_one = " ";
	const spacing_two = "  ";
	
	const list = [];

	const qText = (qty || '').toString().trim().padStart(2, spacing_one);
	const noText = (nominal || '').toString().trim().padStart(MAX_VALUE_L, spacing_one);
	
	let naText = (name || '').toString().trim();

	let remaining = MAX_CHAR;
	
	// subtract qty
	remaining -= qText.length;
	// sutract spacing
	remaining -= spacing_one.length;
	// subtract with value and spacing
	remaining -= (spacing_two.length + noText.length);

	let i = 0;
	while(naText.length > 0) {
		const subName = naText.substring(0, remaining).padEnd(remaining, spacing_one);
		if (i === 0) {
			list.push([qText, spacing_one, subName, spacing_two, noText].join(''));
		} else {
			list.push([spacing_two,spacing_one,subName].join(''));
		}
		naText = naText.substring(subName.length, naText.length);
		++i;
	}
	return list;
}
const buildTextPenjualanHarian = ({ qty = '', name = '', nominal = '' }, ukuran = 'kecil') => {
	const MAX_CHAR = MAX_SMALL_CHAR;
	const MAX_VALUE_L = 9;
	const spacing_one = " ";
	const spacing_two = "  ";
	
	const list = [];

	const qText = (qty || '').toString().trim().padStart(3, spacing_one);
	const noText = (nominal || '').toString().trim().padStart(MAX_VALUE_L, spacing_one);
	
	let naText = (name || '').toString().trim();

	let remaining = MAX_CHAR;
	
	// subtract qty
	remaining -= qText.length;
	// sutract spacing
	remaining -= spacing_one.length;
	// subtract with value and spacing
	remaining -= (spacing_two.length + noText.length);

	let i = 0;
	while(naText.length > 0) {
		const subName = naText.substring(0, remaining).padEnd(remaining, spacing_one);
		if (i === 0) {
			list.push([qText, spacing_one, subName, spacing_two, noText].join(''));
		} else {
			list.push([spacing_two,spacing_one,subName].join(''));
		}
		naText = naText.substring(subName.length, naText.length);
		++i;
	}
	return list;
}

module.exports.check = (query = {}, callback) => {
	let status_code = 200;
	let message			= "OK";
	let result			= {};
	
	const { ukuran } = query;
	const dict = { timestamp: Date.now(), is_print: query.printing === 'true' };
	const printer = new escpos.Printer(device, options);
	// console.log(dict, query);

	dict.timestampString = (dict.timestamp.toString()).padStart(BARCODE_MAX_LENGTH, '0');
	const logo = ukuran === "besar" ? logoBig : logoSmall;
	async.waterfall([
		(flowCallback) => escpos.Image.load(logo, (image) => {
			dict.image = image; return flowCallback();
		}),
		(flowCallback) => {
			if (_.isEmpty(device)) { return flowCallback('Printer not found!!!');}
			try {
				device.open(() => {
					if (dict.is_print) {
						printer
							.raster(dict.image)
							.align('CT')
							.text('\n PRINTER SIAP DIGUNAKAN \n')
							.barcode(dict.timestampString, BARCODE_TYPE, { width: 2 })
							.text(moment.tz(dict.timestamp, TIME_ZONE).format('\ndddd, DD MMMM YYYY HH:mm:ss'))
							.text('\n').cut()
					}
					printer.close((a, b) => {
						return flowCallback();
					});
				});
			} catch (error) {
				switch(error.message) {
					case 'LIBUSB_ERROR_NO_DEVICE': {
						device = null;
						setPrinter();
						break;
					}
				}
				return flowCallback(error.message);
			}
		},
	], (err, data) => {
		if (err && err !== "OK") {
			status_code = 400;
			message			= "FAILED";
			result			= err;
		}
		if (_.isNil(err)) {
			console.log('[SUCCESS] Printer is ready');
		} else {
			console.log(`[FAILED] ${err}`);
		}
		return callback({ status_code, message, result });
	});
}
module.exports.printTiket = (body, callback) => {
	let status_code = 200;
	let message			= "OK";
	let result			= {};

	const data = body.list;
	const ukuran = _.get(body, "ukuran", "besar");

	const dict = {};
	const printer = new escpos.Printer(device, options);

	const logo = ukuran === "besar" ? logoBig : logoSmall;
	return async.waterfall([
		(flowCallback) => escpos.Image.load(logo, (image) => {
			dict.image = image; return flowCallback();
		}),
		(flowCallback) => {
			if (_.isEmpty(device)) { return flowCallback(null, 'Printer not found!!!');}
			try { device.open(() => flowCallback()); } catch (error) { return flowCallback(error); }
		},
		(flowCallback) => {
			try {
				data.map((item) => {
					const { title, contents, nomor, tanggal } = item;

					// Set Header
					printer.raster(dict.image);
					printer.font('A').align('CT').style('NORMAL');

					// Set Title
					printer.size(2, 2);
					if (ukuran === "kecil") { printer.size(2, 1); }
					printer.text(` ${title.toUpperCase()}`);
					printer.size(1, 1).text('');

					// Set Content
					printer.size(1, 2);
					if (ukuran === "kecil") { printer.size(1, 1); }
					contents.map((o) => { buildTextTiket(o, ukuran).map(v => {
						// console.log(v, v.length);
						printer.text(v)
					}); });

					// Set footprint
					printer.size(1, 1)
						.text('')
						.text("No Tiket: " + nomor)
						.text(tanggal)
						.text('')
						.cut()
				});
				return printer.close(() => flowCallback());
			} catch (error) {
				console.log({ error });
				return flowCallback(error);
			}
		}
	], (err) => {
		if (err) {
			status_code = 400;
			message			= "FAILED";
			result			= err;
		}
		return callback({ status_code, message, result });
	});
}
module.exports.printPenjualan = (body, callback) => {
	let status_code = 200;
	let message			= "OK";
	let result			= {};

	const max_nominal = 10;
	const ukuran = _.get(body, "ukuran", "kecil");
	
	const nomor = _.get(body, "nomor", "-");
	const pegawai = _.get(body, "pegawai", "-");
	
	const list_transaksi = body.list;

	const total_harga = _.get(body, 'total_harga', '0').toString()
	const total_potongan = _.get(body, 'total_potongan', '0').toString()
	const total = _.get(body, "total", '0').toString()
	
	const total_item = _.get(body, "total_item", '0').toString()
	const total_qty = _.get(body, "total_qty", '0').toString()
	
	const nama = _.get(body, 'nama', '----').toString()
	const alamat = _.get(body, 'alamat', '----').toString()

	const dict = {};
	const printer = new escpos.Printer(device, options);

	const max		= ukuran === 'besar' ? MAX_BIG_CHAR : MAX_SMALL_CHAR;
	const logo 	= ukuran === 'besar' ? logoBig : logoSmall;
	return async.waterfall([
		(flowCallback) => escpos.Image.load(logo, (image) => {
			dict.image = image; return flowCallback();
		}),
		(flowCallback) => {
			if (_.isEmpty(device)) { return flowCallback(null, 'Printer not found!!!');}
			try { device.open(() => flowCallback()); } catch (error) { return flowCallback(error); }
		},
		(flowCallback) => {

			let petugas_line = [
				'Petugas: '.padEnd(max - pegawai.length, ' '),
				pegawai,
			].join('')
			let receipt_line = [
				'Receipt No: '.padEnd(max - nomor.length, ' '),
				nomor,
			].join('')

			let total_harga_line = '   SUB TOTAL  ';
			total_harga_line = total_harga_line.padEnd(max - max_nominal, ' ');
			total_harga_line += total_harga.padStart(max_nominal, ' ');

			let total_potongan_line = '   POTONGAN  ';
			total_potongan_line = total_potongan_line.padEnd(max - max_nominal, ' ');
			total_potongan_line += total_potongan.padStart(max_nominal, ' ');
			
			let total_line = 'TOTAL ';
			total_line = total_line.padEnd((max / 2) - max_nominal, ' ');
			total_line += total.padStart(max_nominal, ' ');

			let total_qty_item_line = `Total Qty: ${total_qty}`;
			total_qty_item_line = [
				`Total Item: ${total_item}`.padEnd(max - total_qty_item_line.length, ' '),
				total_qty_item_line,
			].join('');

			try {

				/**
				 * SET HEADER
				 */
				printer
					.size(1, 1)
					.align('CT')
					.text(nama)
					.text(alamat)
					.text('\n')
				printer
					.align('LT').text(petugas_line)
					.align('LT').text(receipt_line)
				printer.size(1, 1).text(_.times(max, () => '-').join('')); // STRAIGHT LINE
				
				/**
				 * SET LIST PEMBELIAN
				 */
				list_transaksi.map((item) => {
					const list_text = buildTextPenjualan(item);
					printer
						.font('A')
						.align('LT')
					list_text.map(text => printer.text(text))
				});
				
				printer
					.text(_.times(max, () => '-').join('')) // STRAIGHT LINE
					.text(total_harga_line)
					.text(total_potongan_line)
					.text(_.times(max, () => '-').join('')) // STRAIGHT LINE		
				
				printer
					.size(2, 1).text(total_line)
					.size(1, 1).text(total_qty_item_line)
					.text('')
					.align("CT")
					.text(moment.tz(dict.timestamp, TIME_ZONE).format('\ndddd, DD/MM/YYYY HH:mm:ss'))
					.text('Terima Kasih')

				printer.cut();
				return printer.close(() => flowCallback());
			} catch (error) {
				console.log({ error });
				return flowCallback(error);
			}
		}
	], (err) => {
		if (err) {
			status_code = 400;
			message			= "FAILED";
			result			= err;
		}
		return callback({ status_code, message, result });
	});
}
module.exports.printPenjualanHarian = (body, callback) => {
	let status_code = 200;
	let message			= "OK";
	let result			= {};

	const max_nominal = 10;
	const ukuran = _.get(body, "ukuran", "kecil");

	const pegawai = _.get(body, "pegawai", "-");
	
	const list_transaksi = body.list;

	const total_harga = _.get(body, 'total_harga', '0').toString()
	const total_potongan = _.get(body, 'total_potongan', '0').toString()
	const total = _.get(body, "total", '0').toString()
	
	const total_item = _.get(body, "total_item", '0').toString()
	const total_qty = _.get(body, "total_qty", '0').toString()
	
	const nama = 'REKAP PENJUALAN HARIAN'

	const dict = {};
	const printer = new escpos.Printer(device, options);

	const max		= ukuran === 'besar' ? MAX_BIG_CHAR : MAX_SMALL_CHAR;
	const logo 	= ukuran === 'besar' ? logoBig : logoSmall;
	return async.waterfall([
		(flowCallback) => escpos.Image.load(logo, (image) => {
			dict.image = image; return flowCallback();
		}),
		(flowCallback) => {
			if (_.isEmpty(device)) { return flowCallback(null, 'Printer not found!!!');}
			try { device.open(() => flowCallback()); } catch (error) { return flowCallback(error); }
		},
		(flowCallback) => {

			let petugas_line = [
				'Petugas: '.padEnd(max - pegawai.length, ' '),
				pegawai,
			].join('')

			let total_harga_line = '   SUB TOTAL  ';
			total_harga_line = total_harga_line.padEnd(max - max_nominal, ' ');
			total_harga_line += total_harga.padStart(max_nominal, ' ');

			let total_potongan_line = '   POTONGAN  ';
			total_potongan_line = total_potongan_line.padEnd(max - max_nominal, ' ');
			total_potongan_line += total_potongan.padStart(max_nominal, ' ');
			
			let total_line = 'TOTAL ';
			total_line = total_line.padEnd((max / 2) - max_nominal, ' ');
			total_line += total.padStart(max_nominal, ' ');

			let total_qty_item_line = `Total Qty: ${total_qty}`;
			total_qty_item_line = [
				`Total Item: ${total_item}`.padEnd(max - total_qty_item_line.length, ' '),
				total_qty_item_line,
			].join('');

			try {

				/**
				 * SET HEADER
				 */
				printer
					.size(1, 1)
					.align('CT')
					.text(nama)
					.text('\n')
				printer
					.align('LT').text(petugas_line)
				printer.size(1, 1).text(_.times(max, () => '-').join('')); // STRAIGHT LINE
				
				/**
				 * SET LIST PEMBELIAN
				 */
				list_transaksi.map((item) => {
					const list_text = buildTextPenjualanHarian(item);
					printer
						.font('A')
						.align('LT')
					list_text.map(text => printer.text(text))
				});
				
				printer
					.text(_.times(max, () => '-').join('')) // STRAIGHT LINE
					.text(total_harga_line)
					.text(total_potongan_line)
					.text(_.times(max, () => '-').join('')) // STRAIGHT LINE		
				
				printer
					.size(2, 1).text(total_line)
					.size(1, 1).text(total_qty_item_line)
					.text('')
					.align("CT")
					.text(moment.tz(dict.timestamp, TIME_ZONE).format('\ndddd, DD/MM/YYYY HH:mm:ss'))
					.text('Terima Kasih')

				printer.cut();
				return printer.close(() => flowCallback());
			} catch (error) {
				console.log({ error });
				return flowCallback(error);
			}
		}
	], (err) => {
		if (err) {
			status_code = 400;
			message			= "FAILED";
			result			= err;
		}
		return callback({ status_code, message, result });
	});
}