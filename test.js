'use strict';
require('dotenv').config();

const _ = require('lodash');
const path = require('path');
const escpos = require('escpos');
const moment = require('moment-timezone');

moment.locale('id');

const device = new escpos.USB();
const printer = new escpos.Printer(device);

const {
	TIME_ZONE,
	BARCODE_TYPE,
	BARCODE_MAX_LENGTH,
} = process.env;

console.log(BARCODE_TYPE, BARCODE_MAX_LENGTH);
const timestamp = Date.now();

const list_barang = [
	{ qty: 1, nominal: '0', name: '++Water 500ml' },
	{ qty: 1, nominal: '145.000', name: 'Bugsy Fried Rice' },
	{ qty: 1, nominal: '150.000', name: 'Corona Extra' },
	{ qty: 1, nominal: '3.500.000', name: 'Devils Lair Cab Sav' },
	{ qty: 1, nominal: '50.000', name: 'Dunhil Putih' },
	{ qty: 1, nominal: '4.200.000', name: 'Inniskilin Cabernet' },
	{ qty: 1, nominal: '15.000', name: 'Lighter' },
	{ qty: 1, nominal: '8.800.000', name: 'Macallan 18 Sher Btl' },
	{ qty: 1, nominal: '50.000', name: 'Marlboro Ice Blast' },
	{ qty: 1, nominal: '9.200.000', name: 'Martell X0 Btl' },
	{ qty: 1, nominal: '65.000', name: 'Mushroom Soup' },
	{ qty: 1, nominal: '175.000', name: '++Water 500ml' },
	{ qty: 1, nominal: '100.000', name: 'Marlboro Light' },
	{ qty: 1, nominal: '100.000', name: 'Marlboro Merah' },
	{ qty: 1, nominal: '100.000', name: 'Sampoerna Mild Merah' },
	{ qty: 1, nominal: '165.000', name: 'Coca Cola Botol' },
	{ qty: 1, nominal: '2.400.000', name: 'Macallan 18 Sher Btl' },
	{ qty: 1, nominal: '1.250.000', name: 'Saltram No.1 Shiraz' },
	{ qty: 1, nominal: '150.000', name: 'Sampoerna Mild Ment' },
	{ qty: 1, nominal: '580.000', name: 'Bugsy Fried Rice' },
	{ qty: 1, nominal: '60.000', name: 'Telur' },
	{ qty: 1, nominal: '7.800.000', name: 'Whistler L Chardonay' },
	{ qty: 1, nominal: '325.000', name: 'Singkong Lada Garam' },
	{ qty: 1, nominal: '1.420.000', name: 'Fruit Platter' },
	{ qty: 1, nominal: '750.000', name: 'Martabak Telor' },
];

const MAX_BIG_CHAR = 44;
const MAX_SMALL_CHAR = 32;
const buildText = ({ qty = '', name = '', nominal = '' }, ukuran = 'kecil') => {
	const MAX_CHAR = MAX_SMALL_CHAR;
	const MAX_VALUE_L = 9;
	const spacing_one = " ";
	const spacing_two = "  ";
	
	const list = [];

	const qText = (qty || '').toString().trim().padStart(2, spacing_one);
	const noText = (nominal || '').toString().trim().padStart(MAX_VALUE_L, spacing_one);
	
	let naText = (name || '').toString().trim();

	let remaining = MAX_CHAR;
	
	// subtract qty
	remaining -= qText.length;
	// sutract spacing
	remaining -= spacing_one.length;
	// subtract with value and spacing
	remaining -= (spacing_two.length + noText.length);

	let i = 0;
	while(naText.length > 0) {
		const cut = naText.substring(0, remaining).padEnd(remaining, spacing_one);
		if (i === 0) {
			list.push([qText, spacing_one, cut, spacing_two, noText].join(''));
		} else {
			list.push([spacing_two,spacing_one,cut].join(''));
		}
		naText = naText.substring(cut.length, naText.length);
		++i;
	}
	return list;
}

device.open(async () => {
	const list = _.flatMap(list_barang.map(buildText));
	list.map((text) => {
		console.log(text);
		printer
			.align('LT')
			.font('B')
			.text(text)
	})
	printer.cut();
	printer.close();
});