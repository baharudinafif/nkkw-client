
const path 		= require('path');
const cors 		= require('cors');
const express = require('express')
const bodyParser = require('body-parser')

require('dotenv').config({ path: path.join(__dirname, '.env') });

const printer = require('./printer');

const app = express();
const port = process.env.APP_PORT || 8000;

const opts = {
	origin: true,
	credentials: true,
};
app.use(cors(opts));
app.options('*', cors(opts));
app.use(bodyParser.json({ limit: '200mb' }));
app.use(bodyParser.urlencoded({ limit: '200mb', extended: true }));

app.get('/cek', (req, res) => {
	printer.check(req.query, (data) => res.status(data.status_code).json(data));
});
app.post('/cetak-tiket', (req, res) => {
	printer.printTiket(req.body, (data) => res.status(data.status_code).json(data));
});
app.post('/cetak-penjualan', (req, res) => {
	printer.printPenjualan(req.body, (data) => res.status(data.status_code).json(data));
});
app.post('/cetak-penjualan-harian', (req, res) => {
	printer.printPenjualanHarian(req.body, (data) => res.status(data.status_code).json(data));
});

app.listen(port, () => console.log(`Listening on port ${port}!`))
