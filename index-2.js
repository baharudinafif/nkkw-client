'use strict';
require('dotenv').config();
const path = require('path');
const escpos = require('escpos');
const moment = require('moment-timezone');

moment.locale('id');

const device = new escpos.USB();
const printer = new escpos.Printer(device);

const {
	TIME_ZONE,
	BARCODE_TYPE,
	BARCODE_MAX_LENGTH,
} = process.env;

console.log(BARCODE_TYPE, BARCODE_MAX_LENGTH, process.env);
const timestamp = Date.now();

// const logo = path.join(__dirname, 'logo-big.png');
// escpos.Image.load(logo, function (image) {
// 	const ts = (timestamp + '').toString().padStart(BARCODE_MAX_LENGTH, '0');
	
// 	device.open(async () => {
// 		await printer.raster(image);
// 		printer
// 			.align('ct')
// 			.font('A')
// 			.align('CT')
// 			.style('b').text('\nSewa Ban\n')
// 			.size(2, 2)
// 			.style('B').text('3 Buah')
// 			.size(1, 1)
// 			.text('\n')
// 			.barcode(ts, BARCODE_TYPE, { width: 2 })
// 			.text(moment.tz(timestamp, TIME_ZONE).format('\ndddd, DD MMMM YYYY HH:mm:ss'))
// 			.cut()
			
// 		printer.close();
// 	});
// });


console.log(JSON.stringify([
	{ type: "text", value: "Sewa Ban Kecil" },
	{ type: "text", value: "3 Buah", style: "B", size: 2 },
]))